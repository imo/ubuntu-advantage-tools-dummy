# ubuntu-advantage-tools-dummy

A dummy package whose sole purpose is to clean up `ubuntu-advantage-tools` without breaking the dependencies of other packages that depend on it for no reason at all.

Oh wait there is a reason?

```
Random ubuntu developer: I-it's a better experience for the user if the tools are installed instead of getting "command not found".
```

Yeah yeah, cut the crap, we are talking about Ubuntu server here, not desktop. There is a good chance the user is an admin or at least a person with technical background who is capable of installing a package.
